from django.apps import AppConfig


class FiturForumConfig(AppConfig):
    name = 'fitur_forum'
