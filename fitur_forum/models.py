from django.db import models

# Create your models here.
class Pengguna(models.Model):
    kode_identitas = models.CharField('Kode Identitas', max_length=20, primary_key=True, )
    nama = models.CharField('Nama', max_length=200)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

class Message(models.Model):
	pengguna = models.ForeignKey(Pengguna)
	title = models.CharField(max_length=140)
	message = models.TextField()
	kode_message = models.CharField("Kode Message", max_length=50)
	created_date = models.DateTimeField(auto_now_add=True)


    