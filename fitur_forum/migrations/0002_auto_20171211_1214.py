# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-12-11 05:14
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('fitur_forum', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Pengguna',
            fields=[
                ('kode_identitas', models.CharField(max_length=20, primary_key=True, serialize=False, verbose_name='Kode Identitas')),
                ('nama', models.CharField(max_length=200, verbose_name='Nama')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.RemoveField(
            model_name='message',
            name='email',
        ),
        migrations.RemoveField(
            model_name='message',
            name='name',
        ),
        migrations.AddField(
            model_name='message',
            name='pengguna',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='fitur_forum.Pengguna'),
            preserve_default=False,
        ),
    ]
