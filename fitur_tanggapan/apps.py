from django.apps import AppConfig


class FiturTanggapanConfig(AppConfig):
    name = 'fitur_tanggapan'
